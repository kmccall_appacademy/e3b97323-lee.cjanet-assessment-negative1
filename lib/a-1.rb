# first try 23 examples, 16 fail 9:21p - 10:06p


# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  missing_nums = []
  ((nums.first)...(nums.max)).each do |num| missing_nums << num if !nums.include?(num)
  end
  missing_nums
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  sum = 0
  binary.reverse.each_char.with_index do |num, idx|
    sum+= num.to_i*(2**idx)
  end
  sum
end



class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    keys = self.keys
    values = self.values
    i = 0
    while i < keys.length
      self.delete(keys[i]) if prc.call(keys[i], values[i]) == false
      i+=1
    end
    self
  end



end



class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.


  def my_merge(hash, &prc)
    i=0
    while i < hash.length
      h_key = hash.keys[i]
      if self.has_key?(h_key) && !prc.nil?
        self[h_key] = prc.call(h_key, self[h_key], hash[h_key])
      else
        self[h_key] = hash[h_key]
      end
      i+=1
    end
    self
  end


end




# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  n.abs == n ? lucas_positive(n) : lucas_negative(n)
end

def lucas_positive(n)
  lucas_series = [2,1]

  lucas_series << lucas_series[-2..-1].reduce(:+) until lucas_series[n] != nil

  lucas_series[n]
end

def lucas_negative(n)
  lucas_series = [2,1]

  until lucas_series.length == n.abs+2
    lucas_series.unshift(lucas_series[1] - lucas_series[0])
  end

  lucas_series[0]
end




# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  (3...string.length).to_a.reverse.each do |range|
    (string.length - range).times do |i|
      word = string[i..i+range]
      return word.length if palindrome?(word)
    end
  end
  false
end

def palindrome?(word)
  word.reverse == word
end
